<?php
    // start session
    session_start();

    // check if session exist
    if(!empty($_SESSION)){
        // dstroy session on logout
        if(isset($_POST['btn_logout'])) {
            session_destroy();
            $target_page = dirname($_SERVER['PHP_SELF']) . '/1-13.php';
            header("Location: $target_page");
        }
    }else{
        // redirect to login page
        $target_page = dirname($_SERVER['PHP_SELF']) . '/1-13.php';
        header("Location: $target_page");
    }
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid lightgrey;
    text-align: left;
    padding: 8px;
}

th {
    background-color: black;
    color: white;
}

</style>
<body>

    <table>
        <tr>
            <th>Name</th>
            <th>Age</th>
            <th>Email</th>
            <th>Image</th>
        </tr>
        <tr>
            <?php
                echo '<td>'.$_SESSION["name"].'</td>';
                echo '<td>'.$_SESSION["age"].'</td>';
                echo '<td>'.$_SESSION["email"].'</td>';
                echo '<td>'.$_SESSION["image"].'</td>';
            ?>
            
            
        </tr>
    </table>

    <div style="float:right; margin-bottom: 20px; margin-top: 20px;">
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" >
            <input type="submit" name="btn_logout" id="btn_logout" value="Logout">
        </form>
    </div>

</body>
</html>