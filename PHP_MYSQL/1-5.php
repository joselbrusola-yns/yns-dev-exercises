<?php

// 1-5: Input date. Then show 3 days from the input date and the days of the week of each date.
// Display 1 text box in a page. If you enter the date in the text box and press the submit button, it will calculate and show the result.

$result = '';
$error_message = '';
if (isset($_POST['btn_submit'])){

    // variable declaration
    $input_date = $_POST['input_date'];

    // check if input date is empty
    if($input_date != '' || $input_date != null){
        // intialize date as date object
        $date = new DateTime($input_date);

        // calclate date
        for ($i = 1; $i <= 3; $i++) {
            $date->modify('+1 day');
            $result.= $date->format('Y-m-d l') . '<br>';
        }
    }else{
        $error_message = '<p style="color:red">Please select a date.</p>';
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">

        <p><b>Input date. Then show 3 days from the input date and the days of the week of each date.</b></p>

        <input type="date" name="input_date" id="input_date">
        <input type="submit" name="btn_submit" id="btn_submit" value="Submit">

    </form>

    <?php
        if(!$error_message){
            if($result !== '') echo '<p>Result: <br>'.$result.'</p>';
        }else{
            echo $error_message;
        }
    ?>
    
</body>
</html>