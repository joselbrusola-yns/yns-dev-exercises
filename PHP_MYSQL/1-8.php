<?php

// 1-8: Store inputted user information into a CSV file.
// Validate inputted values in the form you created. Then show error messages if there is any error.Try to use regular expressions when you validate the mail address.

$name = '';
$age = '';
$email = '';

if (isset($_POST['btn_submit'])){

    // put submit value into a new variable to have copy before unset on the csv saving part
    $submit_button = $_POST['btn_submit'];

    // variable declaration
    // sanitize input
    $name = preg_replace('/\s+/', ' ', filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS)); // remove extra white space
    $age = filter_input(INPUT_POST, 'age', FILTER_SANITIZE_SPECIAL_CHARS);
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_SPECIAL_CHARS);

    // validate inputs
    $validation_result = validate($name, $age, $email);

    // save data to csv if there is no error in validation
    if(empty($validation_result)){

        // unset post value that is not needed
        unset($_POST['btn_submit']);

        // set header
        $headers=array_keys($_POST);
        // set file name
        $file_name = "userinfo.csv";

        $csvfile = fopen($file_name, 'a+');

        // get csv data as array for checking
        $csv = fgetcsv($csvfile); 

        // check if csv is empty
        if(empty($csv)) {
            // write header part
            fputcsv($csvfile, $headers );
        }
        
        // write main content
        fputcsv($csvfile, $_POST );

        fclose($csvfile);
    }

}

function validate($name, $age, $email){

$error_message = [];

// validate name
    // check if not empty
    if ($name == null || $name == ' ') {
        $error_message[] = "Name is required.";
    } else {
        // character only
        $valid_pattern = '/[^a-z ]/i';
        if (preg_match($valid_pattern, $name)) {
            $error_message[] = "Invalid name: Letters and space only.";
        }
    }
// validate age
    // check if not empty
    if ($age == null || $age === '') {
        $error_message[] = "Age is required.";
    } else {
        // is numeric
        if (!is_numeric($age)) {
            $error_message[] = "Invalid age: Numbers only.";
        }else{
            // check age length more than 4 is invalid
            if(strlen($age) > 3){
                $error_message[] = "Invalid age: Maximum age length is 3 digit only.";
            }

        }
    }
    
// validate email
    // check if not empty
    if ($email == null || $email == '') {
        $error_message[] = "Email is required.";
    } else {
        // validate email
        $valid_pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
        if (!preg_match($valid_pattern, $email) == 1) {
            $error_message[] = "Invalid email";
        }
    }
    if($error_message); return $error_message;

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" >

        <p><b>User information</b></p>

        <label for="name">Name</label>
        <input type="text" name="name" id="name" value="<?php echo isset($_POST['name']) ? $_POST['name'] : '' ?>">
        <br>

        <label for="age">Age</label>
        <input type="text" name="age" id="age" value="<?php echo isset($_POST['age']) ? $_POST['age'] : '' ?>">
        <br>

        <label for="email">Email</label>
        <input type="text" name="email" id="email" value="<?php echo isset($_POST['email']) ? $_POST['email'] : '' ?>">
        <br>

        <input type="submit" name="btn_submit" id="btn_submit" value="Submit">
        
    </form>

    <?php 

        // check if validation result is empty
        if(empty($validation_result)){
            if($name && $age && $email !== ''){
                echo "<p style='color: green'>Data successfully added.</p>";
                header('Location: http://localhost/dev_exercise/PHP_HTML/1-9.php');
            }
        }

        // display error message
        if(isset($validation_result)){
            foreach($validation_result as $error){
                echo "<p style='color: red'>$error</p>";
            }
        }
        
    ?>

</body>
</html>