<?php

// 1-13: Create a login form and embed it into the system that you developed.
// Login form must have 2 text boxes for user ID and password. If there is any error in validation, the error message(s) will be shown. After login, user information will be saved in the session and keep login until the user logs out or closes the web browser.

// start session
session_start();

// check if already login
if(!empty($_SESSION)){
    $target_page = dirname($_SERVER['PHP_SELF']) . '/1-13_view.php';
    header("Location: $target_page");
}

$email = '';
$password = '';

if (isset($_POST['btn_submit'])){
    // put submit value into a new variable to have copy before unset on the csv saving part
    $submit_button = $_POST['btn_submit'];

    // variable declaration
    // sanitize input
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_SPECIAL_CHARS);
    $password = preg_replace('/\s+/', ' ', filter_input(INPUT_POST, 'password', FILTER_SANITIZE_SPECIAL_CHARS)); // remove extra white space

    // validate inputs and uploads
    $validation_result = validate($email, $password);

}

function validate($email, $password){

    $error_message = [];
        
    // validate email
        // check if not empty
        if ($email == null || $email == '') {
            $error_message[] = "Email is required.";
        } else {
            // validate email
            $valid_pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
            if (!preg_match($valid_pattern, $email) == 1) {
                $error_message[] = "Invalid email";
            }
        }

    // validate password
        // check if not empty
        if ($password == null || $password == ' ') {
            $error_message[] = "Password is required.";
        }
    
    // validate email and password
    // check if file exist
    $file_name = "userinfo.csv";
    if (file_exists($file_name)) {
        // open file and read
        $csvfile = fopen($file_name, 'r');

        $credentials = [];

        // row counter
        $row = 1;
        // loop through lines
        while(($lines = fgetcsv($csvfile)) !== false){

            // skip header
            if($row == 1 ){
                $row++;
                continue;
            }

            // add data on new array
            array_push($credentials, $lines);
            $row++;
        
        }
        fclose($csvfile);

        $email_exists = false;
        $credentials_count = count($credentials) - 1;

        $single_user_data = [];

        for($i=0; $i <= $credentials_count; $i++){
            if(in_array($email, $credentials[$i]) ){
                $email_exists = true;
                $single_user_data = $credentials[$i];
            }
        }

        // get the user hashed password
        $verified_password = $single_user_data[3];

        if($email_exists){
            if(!password_verify($password, $verified_password)) {
                $error_message[] = "Password dont match";
            }else{
                // set session data
                $_SESSION["name"] = $single_user_data[0];
                $_SESSION["age"] = $single_user_data[1];
                $_SESSION["email"] = $single_user_data[2];
                $_SESSION["image"] = $single_user_data[4];
            }
        }
    }

    // intialize result array
    $result_array = [];

    // return error message if there is an error
    if($error_message){
        $result_array['error_message'] = $error_message;
    }

    return $result_array;

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" enctype="multipart/form-data">

        <p><b>Sign in</b></p>

        <label for="email">Email</label>
        <input type="text" name="email" id="email" value="<?php echo isset($_POST['email']) ? $_POST['email'] : '' ?>">
        <br>

        <label for="password">Password</label>
        <input type="password" name="password" id="password" value="<?php echo isset($_POST['password']) ? $_POST['password'] : '' ?>">
        <br>

        <input type="submit" name="btn_submit" id="btn_submit" value="Login">
        
    </form>

    <?php 

        // check if validation result is empty
        if(empty($validation_result['error_message'])){
            if($email && $password !== '') {
                echo "<p style='color: green'>Data successfully added.</p>";
                $target_page = dirname($_SERVER['PHP_SELF']) . '/1-13_view.php';
                header("Location: $target_page");
            }
        }

        // display error message
        if(isset($validation_result['error_message'])){
            foreach($validation_result['error_message'] as $error){
                echo "<p style='color: red'>$error</p>";
            }
        }
        
    ?>

</body>
</html>