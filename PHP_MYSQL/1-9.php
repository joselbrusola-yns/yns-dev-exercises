<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid lightgrey;
    text-align: left;
    padding: 8px;
}

th {
    background-color: black;
    color: white;
}

tr:nth-child(even) {
  background-color: lightgrey;
}
</style>
<body>

    <table>
        <tr>
            <?php 

                // 1-9: Show the user information using table tags.
                // Create a new page for listing up all the user information (from the csv) using table tags.

                // check if file exist
                $file_name = "userinfo.csv";
                if (file_exists($file_name)) {
                    // open file and read
                    $csvfile = fopen($file_name, 'r');

                    // row counter
                    $row = 1;
                    // loop through lines
                    while(($lines = fgetcsv($csvfile)) !== false){

                    // populate table
                    echo "<tr>";
                
                        foreach($lines as $key => $val){
                            echo "<td>" . $val . "</td>";
                        }
                        $row++;

                    echo '</tr>';
                    }

                    fclose($csvfile);
                }else{
                    // display if csv file does not exists
                    echo "<tr><td>No data to display.</td></tr>";
                }
                
            ?>
        </tr>
    </table>

</body>
</html>