<?php 

// 1-3: Show the greatest common divisor.
// Display 2 text boxes in a page. If you enter numbers in the text box and press the submit button, it will calculate and show the result.

$result = '';
$error_message = '';
if (isset($_POST['btn_submit'])){

    // variable declaration
    // sanitize input then convert to int
    $input_1 = (int)filter_input(INPUT_POST, 'input_1', FILTER_SANITIZE_SPECIAL_CHARS); 
    $input_2 = (int)filter_input(INPUT_POST, 'input_2', FILTER_SANITIZE_SPECIAL_CHARS);

    // validate input
    if($input_1 > 0 && $input_2 > 0){
        // calculate GCD
        $result = gcd($input_1, $input_2);
    }else{
        $error_message = '<p style="color:red">Invalid input: Numbers greater than 0 only.</p>';
    }

}

// Recursive function for gcd
function gcd ($input_1, $input_2) {
    return ($input_1 % $input_2) ? gcd($input_2, $input_1 % $input_2) : $input_2;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">

        <p><b>Show the greatest common divisor of 2 positive numbers.</b></p>

        <input type="text" name="input_1" id="input_1">
        <input type="text" name="input_2" id="input_2">
        <input type="submit" name="btn_submit" id="btn_submit" value="Submit">

        <?php
            if(!$error_message){
                if($result !== '') echo '<p>Result: '.$result.'</p>';
            }else{
                echo $error_message;
            }
        ?>

    </form>
    
</body>
</html>