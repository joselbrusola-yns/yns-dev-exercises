<?php 

// 1-6: Input user information. Then show it on the next page.
// Create a form for basic user information. When you enter values in each input then press the submit button, it will show the inputted data in another page.

$name = '';
$age = '';
$email = '';

if (isset($_POST['btn_submit'])){

    // variable declaration
    // sanitize input
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS);
    $age = filter_input(INPUT_POST, 'age', FILTER_SANITIZE_SPECIAL_CHARS);
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_SPECIAL_CHARS);

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" target="_blank">

        <p><b>User information</b></p>

        <label for="name">Name</label>
        <input type="text" name="name" id="name">
        <br>

        <label for="age">Age</label>
        <input type="text" name="age" id="age">
        <br>

        <label for="email">Email</label>
        <input type="text" name="email" id="email">
        <br>

        <input type="submit" name="btn_submit" id="btn_submit" value="Submit">
        
    </form>
    
    <?php
    
        if($name && $age && $email !== ''){
            echo '<p>Result: </p>';
            echo  "<p>Name: $name</p>";
            echo  "<p>Age: $age</p>";
            echo  "<p>Email: $email</p>";
        }
        
    ?>

</body>
</html>