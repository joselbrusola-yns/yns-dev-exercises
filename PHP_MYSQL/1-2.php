<?php
// 1-2: The four basic operations of arithmetic.
// Display 2 text boxes in a page. If you enter numbers in the text box and press one of the submit buttons, it will calculate and show the result according to the operation.

$result = '';
$error_message = '';
if (isset($_POST['btn_submit'])){

    // variable declaration
    $operation = $_POST['operation'];
    // sanitize input then convert to int
    $input_1 = filter_input(INPUT_POST, 'input_1', FILTER_SANITIZE_SPECIAL_CHARS); 
    $input_2 = filter_input(INPUT_POST, 'input_2', FILTER_SANITIZE_SPECIAL_CHARS);

    // check if integer first. Reason is_numeric of '-1' return false
    if (filter_var($input_1, FILTER_VALIDATE_INT) && filter_var($input_2, FILTER_VALIDATE_INT) !== false ) {
        // handle operation
        $result = calculate($operation, $input_1, $input_2);

    }else{
        // check for float numbers that is not a valid integer using filter_var([variables],FILTER_VALIDATE_INT)
        if(is_numeric($input_1) && is_numeric($input_2)){
            // handle operation
            $result = calculate($operation, $input_1, $input_2);
        }else{
            $error_message = '<p style="color:red">Invalid input: Numbers only.</p>';
        }
    }

}

// calculate result
function calculate ($operation, $input_1, $input_2) {
    // handle operation
    switch ($operation) {
        case 'addition':
            $result = $input_1 + $input_2;
            break;
        case 'subtraction':
            $result = $input_1 - $input_2;
            break;
        case 'multiplication':
            $result = $input_1 * $input_2;
            break;
        case 'division':
            $result = $input_1 / $input_2;
            break;
    }

    return $result;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">

        <p><b>Don't forget to change operation. (addition is the default)</b></p>

        <input type="text" name="input_1" id="input_1">
        <select name="operation" id="operation">
            <option value="addition">+</option>
            <option value="subtraction">-</option>
            <option value="multiplication">*</option>
            <option value="division">/</option>
        </select>
        <input type="text" name="input_2" id="input_2">
        <input type="submit" name="btn_submit" id="btn_submit" value="Submit">

        <?php 
            if(!$error_message){
                if($result !== '') echo '<p>Result: '.$result.'</p>';
            }else{
                echo $error_message;
            }
        ?>
        
    </form>
    
</body>
</html>