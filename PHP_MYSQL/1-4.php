<?php

// 1-4: Solve the FizzBuzz problem.
// Display 1 text box in a page. If you enter a number, which means last number, in the text box and press the submit button, it will calculate and show the result. Print each number from 1 up to the submitted number on a new line. For each multiple of 3, print "Fizz" instead of the number. For each multiple of 5, print "Buzz" instead of the number. For numbers which are multiples of both 3 and 5, print "FizzBuzz" instead of the number.

$result = '';
$error_message = '';
if (isset($_POST['btn_submit'])){

    // variable declaration
    // sanitize input then convert to int
    $input = (int)filter_input(INPUT_POST, 'input', FILTER_SANITIZE_SPECIAL_CHARS);

    // validate input
    if($input > 0){
        
        for ($i = 1; $i <= $input; $i++) {
            $isFizz = (0 === $i % 3);
            $isBuzz = (0 === $i % 5);
    
            if (!$isFizz && !$isBuzz) {
                $result.= $i . '<br>';
    
                continue;
            }
    
            if ($isFizz) {
                $result.= 'Fizz';
            }
    
            if ($isBuzz) {
                $result.= 'Buzz';
            }
    
            $result.= '<br>';
        }

    }else{
        $error_message = '<p style="color:red">Invalid input: Numbers greater than 0 only.</p>';
    }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">

        <p><b>Solve the FizzBuzz problem.</b></p>

        <input type="text" name="input" id="input">
        <input type="submit" name="btn_submit" id="btn_submit" value="Submit">

        <?php
            if(!$error_message){
                if($result !== '') echo '<p>Result: <br>'.$result.'</p>';
            }else{
                echo $error_message;
            }
        ?>
    </form>
    
</body>
</html>