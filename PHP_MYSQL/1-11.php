<?php

// // 1-11: Show uploaded images in the table.
// // Add uploaded images in the list page.

$name = '';
$age = '';
$email = '';
$password = '';

if (isset($_POST['btn_submit'])){
    // put submit value into a new variable to have copy before unset on the csv saving part
    $submit_button = $_POST['btn_submit'];

    // variable declaration
    // sanitize input
    $name = preg_replace('/\s+/', ' ', filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS)); // remove extra white space
    $age = filter_input(INPUT_POST, 'age', FILTER_SANITIZE_SPECIAL_CHARS);
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_SPECIAL_CHARS);
    $password = preg_replace('/\s+/', ' ', filter_input(INPUT_POST, 'password', FILTER_SANITIZE_SPECIAL_CHARS)); // remove extra white space

    // validate inputs and uploads
    $validation_result = validate($name, $age, $email, $password);

    // hash password
    $hashed_password = password_hash($password, PASSWORD_DEFAULT);
    // set hashed password
    $_POST['password'] = $hashed_password;

    // save data to csv if there is no error in validation
    if(empty($validation_result['error_message'])){

        // merge post data and the directory of uploads
        if(!empty($validation_result['upload_final_dir'])){
            $final_array = array_merge($_POST, $validation_result['upload_final_dir']);
        }

        // unset post value that is not needed
        unset($final_array['btn_submit']);

        // set header
        $headers=array_keys($final_array);
        // set file name
        $file_name = "userinfo.csv";

        $csvfile = fopen($file_name, 'a+');

        // get csv data as array for checking
        $csv = fgetcsv($csvfile); 

        // check if csv is empty
        if(empty($csv)) {
            // write header part
            fputcsv($csvfile, $headers );
        }
        
        // write main content
        fputcsv($csvfile, $final_array );

        fclose($csvfile);
    }

}

function validate($name, $age, $email, $password){

    $error_message = [];

    // validate name
        // check if not empty
        if ($name == null || $name == ' ') {
            $error_message[] = "Name is required.";
        } else {
            // character only
            $valid_pattern = '/[^a-z ]/i';
            if (preg_match($valid_pattern, $name)) {
                $error_message[] = "Invalid name: Letters and space only.";
            }
        }
    // validate age
        // check if not empty
        if ($age == null || $age === '') {
            $error_message[] = "Age is required.";
        } else {
            // is numeric
            if (!is_numeric($age)) {
                $error_message[] = "Invalid age: Numbers only.";
            }else{
                // check age length more than 4 is invalid
                if(strlen($age) > 3){
                    $error_message[] = "Invalid age: Maximum age length is 3 digit only.";
                }

            }
        }
        
    // validate email
        // check if not empty
        if ($email == null || $email == '') {
            $error_message[] = "Email is required.";
        } else {
            // validate email
            $valid_pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
            if (!preg_match($valid_pattern, $email) == 1) {
                $error_message[] = "Invalid email";
            }else{
                // check if email already exists

                // check if file exist
                $file_name = "userinfo.csv";
                if (file_exists($file_name)) {
                    // open file and read
                    $csvfile = fopen($file_name, 'r');

                    $credentials = [];

                    // row counter
                    $row = 1;
                    // loop through lines
                    while(($lines = fgetcsv($csvfile)) !== false){

                        // skip header
                        if($row == 1 ){
                            $row++;
                            continue;
                        }

                        array_push($credentials, $lines);
                        $row++;
                    
                    }
                    fclose($csvfile);

                    // check if email exists
                    $credentials_count = count($credentials) - 1;
                    for($i=0; $i <= $credentials_count; $i++){

                        if(in_array($email, $credentials[$i]) ){
                            $error_message[] = "Email already exists.";
                        }
                    }
                }
            }
        }

    // validate password
        // check if not empty
        if ($password == null || $password == ' ') {
            $error_message[] = "Password is required.";
        }

    // handle upload files
    $upload_status = '';
    // validate if user choose file
    if(!empty($_FILES['upload_image']['name'])) {

        $upload_file_name = $_FILES['upload_image']['name'];
        $upload_temp_name = $_FILES['upload_image']['tmp_name'];
        $upload_file_size = $_FILES['upload_image']['size'];

        // create dir to upload and valid file extension 
        $allowed_file_extension = array('png', 'jpg', 'jpeg', 'gif');
        $target_dir = "uploads/";

        // get file ext
        $file_ext_exploded = explode('.', $upload_file_name);
        $file_ext = strtolower(end($file_ext_exploded));

        // finalize file name
        $cur_date_time = date('m-d-Y-His');
        $final_dir = $target_dir.$file_ext_exploded[0].'_'.$cur_date_time.'.'.end($file_ext_exploded);

        // validate file ext
        if(in_array($file_ext, $allowed_file_extension)){
            // validate file size im setting 1mb as max file size
            if($upload_file_size <= 1000000){
                // check if directory exists
                if (!file_exists($target_dir)) {
                    mkdir($target_dir, 0777, true);
                }
                // upload file into a directory return true if success else return false
                $upload_status = move_uploaded_file($upload_temp_name, $final_dir);
            }else{
                $error_message[] = 'Invalid file uploaded: Filesize should not exceed 1mb';
            }
        }else{
            $error_message[] = 'Invalid file uploaded: Allowed files extension are image only.';
        }
    }else{
        $error_message[] = 'Upload file is required.';
    }

    // intialize result array
    $result_array = [];

    // return error message if there is an error
    if($error_message){
        $result_array['error_message'] = $error_message;
    }

    // return upload file data
    if($upload_status == true){
        $result_array['upload_final_dir']['image_directory'] = "<img style='height: 100px; width: 100px;' src='".$final_dir."'>";
    }

    return $result_array;

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" enctype="multipart/form-data">

        <p><b>User information</b></p>

        <label for="name">Name</label>
        <input type="text" name="name" id="name" value="<?php echo isset($_POST['name']) ? $_POST['name'] : '' ?>">
        <br>

        <label for="age">Age</label>
        <input type="text" name="age" id="age" value="<?php echo isset($_POST['age']) ? $_POST['age'] : '' ?>">
        <br>

        <label for="email">Email</label>
        <input type="text" name="email" id="email" value="<?php echo isset($_POST['email']) ? $_POST['email'] : '' ?>">
        <br>

        <label for="password">Password</label>
        <input type="password" name="password" id="password" value="<?php echo isset($password) ? $password : '' ?>">
        <br>

        <br>
        <label for="upload_image">Select image to uplaod:</label>
        <br>
        <input type="file" name="upload_image" id="upload_image">
        <br><br>

        <input type="submit" name="btn_submit" id="btn_submit" value="Submit">
        
    </form>

    <?php 

        // check if validation result is empty
        if(empty($validation_result['error_message'])){
            if($name && $age && $email && $password && $result_array['upload_final_dir']['image_directory'] !== '') {
                echo "<p style='color: green'>Data successfully added.</p>";
                $target_page = dirname($_SERVER['PHP_SELF']) . '/1-12.php';
                header("Location: $target_page");
            }
        }

        // display error message
        if(isset($validation_result['error_message'])){
            foreach($validation_result['error_message'] as $error){
                echo "<p style='color: red'>$error</p>";
            }
        }
        
    ?>

</body>
</html>