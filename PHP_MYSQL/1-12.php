<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid lightgrey;
    text-align: left;
    padding: 8px;
}

th {
    background-color: black;
    color: white;
}

</style>
<body>

    <table>
        <tr>
            <?php 

                // 1-12: Add pagination in the list page.
                // Add pagination in the list page in order to show only 10 user information in each page.

                // check if file exist
                $file_name = "userinfo.csv";
                if (file_exists($file_name)) {
                    // open file and read
                    $csvfile = fopen($file_name, 'r');

                    // count row of csv file then -1 to for the title
                    $csvfile_row_count = count(file($file_name, FILE_SKIP_EMPTY_LINES)) - 1;

                    // set number of data per page
                    $data_per_page = 10;
                    // set current page
                    $current_page = isset($_GET['page']) ? $_GET['page'] : 1;
                    // set total pages
                    $total_pages = ceil($csvfile_row_count / $data_per_page);
                    // set start position
                    $start_position = ($current_page - 1) * $data_per_page;
                    // set end position
                    $end_position = $start_position + $data_per_page;

                    // row counter for geader
                    $row_for_header = 1;
                    // loop through lines to get the header
                    while(($lines = fgetcsv($csvfile)) !== false){
                        
                        if($row_for_header == 1){
                            foreach($lines as $key => $val){
                                echo "<th>" . $val . "</th>";
                            }
                            break;
                        }
                    
                    }

                    // row counter
                    $row = 1;
                    // loop through lines
                    while(($lines = fgetcsv($csvfile)) !== false){

                    // populate table
                    echo "<tr>";

                        // check start position
                        if($row <= $start_position){
                            $row++;
                            continue;
                        }

                        // check end position
                        if($row > $end_position){
                            $row++;
                            break;
                        }

                        // display values
                        foreach($lines as $key => $val){
                            echo "<td>" . $val . "</td>";
                        }

                        $row++;

                    echo '</tr>';
                    
                    }

                    fclose($csvfile);
                    
                }else{
                    // display if csv file does not exists
                    echo "<tr><td>No data to display.</td></tr>";
                }
                
            ?>
        </tr>
    </table>
    <br>
    <div style="float:right; margin-bottom: 20px;">
        <?php 
            // for pagination
            for($i=1; $i <= $total_pages; $i++){
                echo '<a href="?page='.$i.'">Page'.$i.'</a> &nbsp';
            }
        ?>
    </div>

</body>
</html>