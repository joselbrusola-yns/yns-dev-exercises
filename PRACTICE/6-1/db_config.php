<?php 
// define database credentials
define('DB_HOST', 'mysql-server');
define('DB_NAME', 'local_test_env');
define('DB_USER', 'root');
define('DB_PASSWORD', 'secret');

try {
    // create connection
    $dbConnection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

    // check connection
    if ($dbConnection->connect_error) {
        // display error message
        die('Connection Failed: ' . $dbConnection->connect_error);
    }

    // connected

} catch (Exception $error) { 
    // display error message
    die('Connection Failed: ' . $error->getMessage());
}