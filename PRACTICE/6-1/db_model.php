<?php 

class DatabaseModel
{
    public function getQuizData($dbConnection) 
    {
        $sql = 'select * from question q inner join choice c on c.question_id = q.id;';
        $result = mysqli_query($dbConnection, $sql);
        $result_array = mysqli_fetch_all($result, MYSQLI_ASSOC);
        
        return $result_array;
    }
        
    public function getCorrectAnswer($dbConnection) 
    {
        $sql = 'select * from question q inner join choice c on c.question_id = q.id where is_correct = 1;';
        $result = mysqli_query($dbConnection, $sql);
        $result_array = mysqli_fetch_all($result, MYSQLI_ASSOC);
        
        return $result_array;
    }
}