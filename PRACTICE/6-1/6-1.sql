create table question (
    id int(11) NOT NULL AUTO_INCREMENT,
    question varchar(255) NOT NULL,
    PRIMARY KEY (id)
);

create table choice (
    id int(11) NOT NULL AUTO_INCREMENT,
    choice varchar(255) NOT NULL,
    question_id int(11) NOT NULL,
    is_correct int(1) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (question_id) REFERENCES question(id)
);

-- insert questions
insert into question (question) 
values ('Who is the pirate king?'),
    ('Where was Gol D. Roger born?'),
    ('Who is Luffy first main opponent?'),
    ('What is the name of the village Luffy was born in?'),
    ('What Devils Fruit did Luffy eat?'),
    ('What is the name Luffy uses to enter the Colosseum in Dressrosa?'),
    ('What is the name of Straw Hat pirates 1st ship?'),
    ('Who is the navigator of the Straw Hat Pirates?'),
    ('Aside from Ace, Luffy also has another sworn brother who is he?'),
    ('What is the name of the kingdom that is led by the Vinsmoke family?');

-- insert choice
insert into choice (choice, question_id, is_correct) 
values ('Gol D. Roger',1,1),
    ('Whitebeard',1,0),
    ('Kaido',1,0),
    ('East Blue',2,1),
    ('West Blue',2,0),
    ('North Blue',2,0),
    ('Alvida',3,1),
    ('Buggy',3,0),
    ('Arlong',3,0),
    ('Foosha Village',4,1),
    ('Syrup Village',4,0),
    ('Wind Village',4,0),
    ('Hito hito no mi model Nika',5,1),
    ('Gomu gomu no mi',5,0),
    ('Mera mera no mi',5,0),
    ('Lucy',6,1),
    ('Ace',6,0),
    ('Sabo',6,0),
    ('Going Merry',7,1),
    ('Thousand Sunny',7,0),
    ('Puffing Tom',7,0),
    ('Nami',8,1),
    ('Robin',8,0),
    ('Zoro',8,0),
    ('Sabo',9,1),
    ('Zoro',9,0),
    ('Sanji',9,0),
    ('Germa Kingdom',10,1),
    ('Wano Kingdom',10,0),
    ('Zou Kingdom',10,0);