<style>
/* navigation */
.navigation-bar ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
}

.navigation-bar li {
  float: left;
}

.navigation-bar li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

.navigation-bar li a:hover:not(.active) {
  background-color: #111;
}

.navigation-bar .active {
  background-color: #111;
}
</style>
</head>
<body>

    <div class="navigation-bar">
        <ul>
            <li><a class="<?php echo basename($_SERVER['PHP_SELF']) == 'index.php' ? 'active' : '' ?>" href="<?php echo dirname($_SERVER['PHP_SELF']) . '/index.php'; ?>">Home</a></li>
            <li><a class="<?php echo basename($_SERVER['PHP_SELF']) == 'quiz.php' ? 'active' : '' ?>"href="<?php echo dirname($_SERVER['PHP_SELF']) . '/quiz.php'; ?>">Quiz</a></li>
            <li><a class="<?php echo basename($_SERVER['PHP_SELF']) == 'calendar.php' ? 'active' : '' ?>"href="<?php echo dirname($_SERVER['PHP_SELF']) . '/calendar.php'; ?>">Calendar</a></li>
        </ul>
    </div>
