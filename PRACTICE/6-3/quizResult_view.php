<?php 

// 6-1: Quiz with three multiple choices
// Provide 10 questions. Show the score after. Questions should be stored in the database and should be randomized. Create wireframe and schema before developing, put them in [Your Name] (Quiz) sheet.

// start session
session_start();

// check if with score
if(empty($_SESSION)){
    $targetPage = dirname($_SERVER['PHP_SELF']) . '/quiz.php';
    header("Location: $targetPage");
}

// redirect to quiz page
if (isset($_POST['btn_submit'])) {
    session_destroy();
    $targetPage = dirname($_SERVER['PHP_SELF']) . '/quiz.php';
    header("Location: $targetPage");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Onepiece Quiz Result</title>
<style>
    .container {
        display: flex;
        justify-content: center;
    }

    .form-container {
        margin-top: 30px;
        padding: 20px;
        border: 1px solid black;
        width: 500px;

        display: flex;
        flex-direction: column;
    }

    .btn-submit {
        margin-top: 25px;
        background-color: green;
        color: white;
        border: none;
        padding: 10px;
    }
</style>
</head>
<body>
    <div class="container">
        <form class="form-container" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" enctype="multipart/form-data">
            <h2 style="text-align:center; margin-top: 0;">Onepiece Quiz Result!</h2>
            
            <p style="text-align: center;">You're score is <?= $_SESSION["score"]?>/10.</p>

            <input class="btn-submit" type="submit" name="btn_submit" id="btn_submit" value="RETAKE">

        </form>
    </div>
</body>
</html>