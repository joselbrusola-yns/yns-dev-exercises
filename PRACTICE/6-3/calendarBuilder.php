<?php
class CalendarBuilder {  
     
    // constructor
    public function __construct(){     
        $this->navigateToSelf = htmlentities($_SERVER['PHP_SELF']);
    }
     
    // properties
    private $daysOfTheWeek = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
    private $currentYear = 0;
    private $currentMonth = 0;
    private $currentDay = 0;
    private $currentDate = null;
    private $daysInMonth = 0;
    private $navigateToSelf = null;
     
    // build and print calendar
    public function show()
    {
        // define year and month
        $year = null;
        $month = null;
         
        // check year if set
        if ($year == null && isset($_GET['year'])) {
            $year = $_GET['year'];
        } elseif(null==$year) {
            $year = date("Y",time());  
        }          
         
        // check month if set
        if ($month == null && isset($_GET['month'])) {
            $month = $_GET['month'];
        } elseif (null==$month) {
            $month = date("m",time());
        }                  
        
        // assign value to currentYear and currentMonth
        $this->currentYear = $year;
        $this->currentMonth = $month;
        $this->daysInMonth = $this->getDaysInMonth($month, $year);  
         
        $calendarView='<div id="calendar">' . 
                        '<div class="box">' . 
                        $this->createNavigation() . 
                        '</div>' . 
                        '<div class="box-content">' . 
                                '<ul class="label">' . $this->createDaysOfTheWeek().'</ul>';   
                                $calendarView.='<div class="clear"></div>';     
                                $calendarView.='<ul class="dates">';    
                                 
                                $weeksInMonth = $this->getWeeksInMonth($month, $year);
                                // Create weeks in a month
                                for ( $i=0; $i<$weeksInMonth; $i++ ) {
                                    //Create days in a week
                                    for ($j=0;$j<=6;$j++) {
                                        $cellNumber = $i*7+$j;
                                        $calendarView.=$this->createDay($cellNumber);
                                    }
                                }
                                 
                                $calendarView.='</ul>';
                                 
                                $calendarView.='<div class="clear"></div>';     
             
                        $calendarView.='</div>';
                 
        $calendarView.='</div>';
        return $calendarView;   
    }

    // get days in a month
    private function getDaysInMonth($month = null, $year = null)
    {
        // check year if set
        if (($year) == null) {
            $year =  date("Y",time()); 
        }
        
        // check year if set
        if (($month) == null) {
            $month = date("m",time());
        }
    
        return date('t',strtotime($year.'-'.$month.'-01'));
    }

    // create navigation
    private function createNavigation()
    {
        // set next month and year and previous month and year
        $nextMonth = $this->currentMonth == 12 ? 1 : intval($this->currentMonth) + 1;
        $nextYear = $this->currentMonth == 12 ? intval($this->currentYear) + 1 : $this->currentYear;
        $prevMonth = $this->currentMonth == 1 ? 12 : intval($this->currentMonth) - 1;
        $prevYear = $this->currentMonth == 1 ? intval($this->currentYear) - 1 : $this->currentYear;
         
        return
            '<div class="header">' . 
                '<a class="prev" href="' . $this->navigateToSelf . '?month='. sprintf('%02d',$prevMonth) . '&year=' . $prevYear . '"> << Prev </a>' .
                    '<span class="title">' . date('F Y',strtotime($this->currentYear . '-' . $this->currentMonth . '-1')) . '</span>' . 
                '<a class="next" href="' . $this->navigateToSelf . '?month=' . sprintf("%02d", $nextMonth) . '&year=' . $nextYear . '"> Next >> </a>' . 
            '</div>';
    }

    // create days of the week title
    private function createDaysOfTheWeek()
    {  
        $calendarView = '';
        foreach ($this->daysOfTheWeek as $index => $label) {
            $calendarView.='<li class="' . ($label == 'Sun' ? 'end-title' : 'title') . '">' . strtoupper($label) . '</li>';
        }
        return $calendarView;
    }

    // get weeks in a month
    private function getWeeksInMonth($month = null, $year = null)
    {
        // check year if set
        if(($year) == null) {
            $year = date("Y",time()); 
        }
        
        // check month if set
        if(($month == null)) {
            $month = date("m",time());
        }
         
        // set number of days in this month
        $daysInMonths = $this->getDaysInMonth($month, $year);
        
        // set number of weeks in a month
        $numOfweeks = ($daysInMonths % 7 == 0 ? 0 : 1) + intval($daysInMonths / 7);

        // set end of the month
        $monthEndingDay = date('w',strtotime($year . '-' . $month . '-'  .$daysInMonths));
         
        // set start of the month
        $monthStartDay = date('w',strtotime($year . '-' . $month . '-01'));
         
        if ($monthEndingDay < $monthStartDay) {
            $numOfweeks++;
        }
         
        return $numOfweeks;
    }
     
    // create days on the calendar
    private function createDay($cellNumber)
    {
        // check if current day is 0, then assign first day of the week
        if ($this->currentDay == 0) {
             
            // set first day of the week
            $firstDayOfTheWeek = date('w',strtotime($this->currentYear . '-' . $this->currentMonth . '-01'));
            
            if (intval($cellNumber) == intval($firstDayOfTheWeek)) {
                $this->currentDay = 1;
            }
        }
        
        // check if current day not equal to 0, the set current data and assign no cell content
        if (($this->currentDay != 0) && ($this->currentDay <= $this->daysInMonth)) {
            // set current date
            $this->currentDate = date('Y-m-d',strtotime($this->currentYear . '-' . $this->currentMonth . '-' . ($this->currentDay)));
            $cellContent = $this->currentDay;

            $this->currentDay++;

        } else {
            $this->currentDate = null;
            $cellContent = null;
        }
        
        // set current date
        $today = date('Y-m-d');
        
        // set active day and sunday
        return '<li id="li-' . $this->currentDate . '" class="' . (($cellNumber % 7 == 0 ? 'end' : '')) . 
        ($cellContent == null ? 'mask' : '') . (($this->currentDate == $today ? 'active-day' : '')) . '">' . $cellContent . '</li>';
    }
}