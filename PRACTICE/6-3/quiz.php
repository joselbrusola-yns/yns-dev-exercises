<?php

// 6-1: Quiz with three multiple choices
// Provide 10 questions. Show the score after. Questions should be stored in the database and should be randomized. Create wireframe and schema before developing, put them in [Your Name] (Quiz) sheet.

// start session
session_start();

require 'db_config.php';
require 'db_model.php';

$db = new DatabaseModel();

$quizData = $db->getQuizData($dbConnection);
$answerKey = $db->getCorrectAnswer($dbConnection);

// format result
$quizDataView = [];

foreach ($quizData as $key => $val) {
    $quizDataView[$val['question_id']][] = $val;
}

// randomize result
shuffle($quizDataView);

$error_message = '';
// get result
if (isset($_POST['btn_submit'])) {

    // loop through the answer
    for ($i=1; $i <= 10; $i++) {
        // check if all answer is set
        if (isset($_POST['choicesFor'.$i])) {
            ${"answer" . $i} = $_POST['choicesFor'.$i];
        } else {
            $error_message = 'Please answer all the items.';
        }
    }

    // check if there is an error
    if ($error_message == '') {
        // calculate score
        $score = 0;
        $answerKeyCount = count($answerKey) - 1;
        for ($i=0; $i <= $answerKeyCount; $i++) {
            foreach ($answerKey[$i] as $key => $val) {
                $userAnswer = ${"answer" . $i+1};
                if ($key == 'id') {
                    if (in_array($userAnswer, $answerKey[$i])) {
                        $score++;
                    }
                }
            }
        }
        // set score on session and redirect to result page
        $_SESSION["score"] = $score;
        $targetPage = dirname($_SERVER['PHP_SELF']) . '/quizResult_view.php';
        header("Location: $targetPage");
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Onepice Quiz</title>
<style>
    .container {
        display: flex;
        justify-content: center;
    }

    .form-container {
        margin-top: 30px;
        padding: 20px;
        border: 1px solid black;
        width: 500px;

        display: flex;
        flex-direction: column;
    }

    .question {
        margin-bottom: 3px;
        font-weight: bold;
    }

    .choice {
        padding-left: 8px;
    }

    .btn-submit {
        margin-top: 25px;
        background-color: green;
        color: white;
        border: none;
        padding: 10px;
    }
</style>

    <!-- navigation -->
    <?php include('navigation.php'); ?>

    <div class="container">
        <form class="form-container" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" enctype="multipart/form-data">
            <h2 style="text-align:center; margin-top: 0;">Onepiece Quiz!</h2>
            <p style="text-align:center; margin-top: 0;">Note: Make sure you answer all questions because the question reshuffle every submit.</p>

            
            <?php 

                if($error_message != ''){
                    echo "<p style='color: red; margin: 0px; padding-bottom: 5px;'>". $error_message. "</p>";
                }

                // display result
                $itemCount = 1;
                foreach ($quizDataView as $key => $val) {

                    // display question
                    echo '<p class="question">'.$itemCount.'. '.$val[0]['question'].'</p>';

                    // randomize choice
                    shuffle($val);

                    // display choices
                    foreach ($val as $item) {
                        echo '<div class="choice">';
                            echo '<input type="radio" id="'.$item['id'].'" name="choicesFor'.$item['question_id'].'" value="'.$item['id'].'">';
                            echo '<label for="html">'.$item['choice'].'</label><br>';
                        echo '</div>';
                    }

                    $itemCount++;
                }
                
            ?>

            <input class="btn-submit" type="submit" name="btn_submit" id="btn_submit" value="SUBMIT">

        </form>
    </div>
</body>
</html>