<?php 

    // 6-2: Calendar
    // Display calendar of specific month. Provides 2 buttons for forwarding and backwarding month. Default month displayed should be the current month and should highlight the current date.

    include 'calendarBuilder.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calendar</title>
</head>
<style>
/* calendar header */
div#calendar {
    margin:0px auto;
    padding:0px;
    width: 602px;
    font-family: Arial, Helvetica, sans-serif;
    border:1px solid black;
    border-top:none;
    border-radius: 10px;
}

div#calendar div.box{
    position:relative;
    top:0px;
    left:0px;
    width:100%;
    height:40px;
    background-color: black;
    border:1px solid black;
    border-radius: 10px;
}

div#calendar div.header {
    line-height:40px;  
    vertical-align:middle;
    position:absolute;
    left:11px;
    top:0px;
    width:582px;
    height:40px;   
    text-align:center;
}

div#calendar div.header a.prev,div#calendar div.header a.next { 
    position:absolute;
    top:0px;   
    height: 17px;
    display:block;
    cursor:pointer;
    text-decoration:none;
    color:#FFF;
}

div#calendar div.header span.title {
    color:#FFF;
    font-size:18px;
    text-transform: uppercase;
}

div#calendar div.header a.prev {
    left:0px;
}

div#calendar div.header a.next {
    right:0px;
}
    
/* calendar body */
div#calendar ul.label {
    float:left;
    margin: 0px;
    padding: 0px;
    margin-top:5px;
    margin-left: 5px;
}

/* make sunday red */
div#calendar ul.label li.end-title {
    color: red;
}
    
div#calendar ul.label li {
    margin:0px;
    padding:0px;
    margin-right:5px;  
    float:left;
    list-style-type:none;
    width:80px;
    height:40px;
    line-height:40px;
    vertical-align:middle;
    text-align:center;
    color:#000;
    font-size: 15px;
    background-color: transparent;
}
    
div#calendar ul.dates {
    float:left;
    margin: 0px;
    padding: 0px;
    margin-left: 5px;
    margin-bottom: 5px;
}

div#calendar ul.dates li.end {
    color: red;
}

/* active day */
div#calendar ul.dates li.active-day {
    background-color: #000;
    color: white;
}

div#calendar ul.dates li {
    margin:0px;
    padding:0px;
    margin-right:5px;
    margin-top: 5px;
    line-height:80px;
    vertical-align:middle;
    float:left;
    list-style-type:none;
    width:80px;
    height:80px;
    font-size:25px;
    background-color: #ededed;
    color:#000;
    text-align:center; 
}

div#calendar ul.dates :hover {
  background-color: black;
  color: white;
}
    
div.clear{
    clear:both;
}
</style>
<body>
    <h1 style="text-align:center;">Calender</h1>
    <?php
        $calendar = new CalendarBuilder();
        echo $calendar->show();
    ?>
</body>
</html>