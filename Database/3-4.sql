-- 3-4: Solve problems using SQL
-- https://docs.google.com/spreadsheets/d/1jcfbQpZox9qDpOujYIcrtX6vu02b7gxM4-mkA8hbv8A/edit#gid=1560196355

-- CREATE PART

-- create departments table
create table departments (
	id int(11) NOT NULL AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	PRIMARY KEY (id),
	UNIQUE KEY UC_name (name)
);

-- create employees table
create table employees (
	id int(11) NOT NULL AUTO_INCREMENT,
    first_name varchar(255) NOT NULL,
    last_name varchar(255) NOT NULL,
    middle_name varchar(255),
    birth_date date NOT NULL,
    department_id int(11) NOT NULL,
    hire_date date,
    boss_id int(11),
    PRIMARY KEY (id),
    FOREIGN KEY (department_id) REFERENCES departments(id)
);

-- create positions table
create table positions (
	id int(11) NOT NULL AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	PRIMARY KEY (id),
	UNIQUE KEY UC_name (name)
);

-- create employee_positions table
create table employee_positions (
	id int(11) NOT NULL AUTO_INCREMENT,
	employee_id int(11) NOT NULL,
	position_id int(11) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (employee_id) REFERENCES employees(id),
	FOREIGN KEY (position_id) REFERENCES positions(id),
	UNIQUE UC_employee_position (employee_id, position_id)
);


-- INSERT PART

-- insert departments data
insert into departments (name) 
values ('Executive'),
('Admin'),
('Sales'),
('Development'),
('Design'),
('Marketing')

-- insert employees data
insert into employees (first_name, last_name, middle_name, birth_date, department_id, hire_date, boss_id) 
values ('Manabu', 'Yamazaki', null, '1976-03-15', 1, null, null),
('Tomohiko', 'Takasago', null, '1974-05-24', 3, '2014-04-01', 1),
('Yuta', 'Kawakimi', null, '1990-08-13', 4, '2014-04-01', 1),
('Shogo', 'Kubota', null, '1985-01-31', 4, '2014-12-01', 1),
('Lorraine', 'San Jose', 'P.', '1983-10-11', 2, '2015-03-10', 1),
('Haille', 'Dela Cruz', 'A.', '1990-11-12', 3, '2015-02-15', 2),
('Godfrey', 'Sarmenta', 'L.', '1993-09-13', 4, '2015-01-01', 1),
('Alex', 'Amistad', 'F.', '1988-04-14', 4, '2015-04-10', 1),
('Hideshi', 'Ogoshi', null, '1983-07-15', 4, '2014-06-01', 1),
('Kim', '', '', '1977-10-16', 5, '2015-08-06', 0)

-- insert positions data
insert into positions (name) 
values ('CEO'),
('CTO'),
('CFO'),
('Manager'),
('Staff')

-- insert employee_positions data
insert into employee_positions (employee_id, position_id) 
values (1,1),
(1,2),
(1,3),
(2,4),
(3,5),
(4,5),
(5,5),
(6,5),
(7,5),
(8,5),
(9,5),
(10,5)

-- Answers
1. select * from employees where last_name like 'K%';

2. select * from employees where last_name like '%i';

3. select CONCAT(first_name,' ',middle_name,' ',last_name) as full_name, hire_date 
from employees where hire_date between '2015-1-1' and '2015-3-21' order by (hire_date) ASC;

4. select e2.last_name, e1.last_name from employees e1
inner join employees e2 
on e1.id = e2.boss_id;

5. select employees.last_name from employees 
inner join departments 
on employees.department_id = departments.id 
where departments.name = 'Sales' order by (last_name) DESC;

6. select COUNT(middle_name) as count_has_middle from employees where middle_name != '';

7. select UPPER(departments.name), COUNT(employees.department_id) from departments 
inner join employees 
on departments.id = employees.department_id 
GROUP BY departments.name;

8. select first_name, middle_name, last_name, hire_date from employees order by (hire_date) DESC Limit 1;

9. select UPPER(departments.name), '' as id from departments 
left join employees 
on departments.id = employees.department_id 
group by departments.name 
having count(employees.department_id) < 1;

10. select e.first_name, e.middle_name, e.last_name from employees e
inner join employee_positions ep
on e.id = ep.employee_id 
group by ep.employee_id 
having count(ep.position_id) > 2;