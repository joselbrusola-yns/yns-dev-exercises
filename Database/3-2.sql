-- 3-2: CREATE TABLE
-- Create any table using SQL.

-- CREATE TABLE
create table user (
	`id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(255) NOT NULL,
    `gender` varchar(255) NOT NULL,
    `age` int(11) NOT NULL,
    PRIMARY KEY (`id`)
);
