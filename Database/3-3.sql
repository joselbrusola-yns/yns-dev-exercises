-- 3-3: INSERT, UPDATE, DELETE
-- Try inserting, updating and deleting from the table you created.

-- insert
INSERT INTO user (name, gender, age)
VALUES 
    ('Josel', 'male', '25'),
    ('Lufy', 'male', '25'),
    ('Nami', 'male', '26'),
    ('Zoro', 'female', '27');

-- update
UPDATE user SET age = 25 WHERE name = 'nami';

-- delete
DELETE FROM user WHERE name = 'nami';