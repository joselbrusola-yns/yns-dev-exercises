<?php

// 3-5: Use the database in the applications that you developed.
// Same as exercises 1-6 to 1-13 but instead of csv, use the database.

// start session
session_start();

require 'database_config.php';
require 'database_model.php';
require 'validation.php';

define('DB_TABLE_NAME', 'userinfo');

$name = '';
$age = '';
$email = '';
$password = '';
$imageDirectory = '';

if (isset($_POST['btn_submit'])) {

    // variable declaration
    // sanitize input
    $name = preg_replace('/\s+/', ' ', filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS)); // remove extra white space
    $age = filter_input(INPUT_POST, 'age', FILTER_SANITIZE_SPECIAL_CHARS);
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_SPECIAL_CHARS);
    $password = preg_replace('/\s+/', ' ', filter_input(INPUT_POST, 'password', FILTER_SANITIZE_SPECIAL_CHARS)); // remove extra white space

    // validate inputs and uploads
    $validation = new Validation();
    $validationResult = $validation->validateForm($name, $age, $email, $password, $dbConnection);

    // hash password
    $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

    // save data to database
    if (empty($validationResult['error_message'])) {
        // check upload directory
        if (!empty($validationResult['upload_final_dir'])) {
            // initialize image directory
            $imageDirectory = $validationResult['upload_final_dir']['image_directory'];

            // add data to database
            $db = new DatabaseModel();
            $insertResult = $db->insert($dbConnection, DB_TABLE_NAME, $name, $age, $email, $hashedPassword, $imageDirectory);

            if ($insertResult) {
                // get newly added data id
                $newDataId = mysqli_insert_id($dbConnection);
                // select userinfo based on id
                $result = $db->selectSingleData($dbConnection, DB_TABLE_NAME, $newDataId);

                // set session data
                $_SESSION["name"] = $result[0]['name'];
                $_SESSION["age"] = $result[0]['age'];
                $_SESSION["email"] = $result[0]['email'];
                $_SESSION["image_directory"] = $result[0]['image_directory'];
            } else {
                echo 'Error: ' . mysqli_error($dbConnection);
            }
        }
    }
}

// check if validation result is empty
if(empty($validationResult['error_message'])){
    if($name && $age && $email && $password && $imageDirectory !== '') {
        $targetPage = dirname($_SERVER['PHP_SELF']) . '/user_view.php';
        header("Location: $targetPage");
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
<style>
    .container {
        display: flex;
        justify-content: center;
    }

    .form-container {
        margin-top: 50px;
        padding: 20px;
        border: 1px solid black;
        width: 400px;

        display: flex;
        flex-direction: column;
    }

    .img-uploader {
        display: flex;
        border: 1px solid black;
        margin-bottom: 15px;
    }

    .uploader {
        flex-grow: 1;
    }

    .item-input {
        margin-bottom: 8px;
        padding: 5px;
    }

    .btn-submit {
        margin-top: 10px;
        background-color: green;
        color: white;
        border: none;
        padding: 10px;
    }
</style>
</head>
<body>
    <div class="container">
        <form class="form-container" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" enctype="multipart/form-data">
            <h2 style="text-align:center; margin-top: 0;">Sign Up</h2>
            
            <label for="name">Name:</label>
            <input class="item-input" type="text" name="name" id="name" value="<?php echo isset($_POST['name']) ? $_POST['name'] : '' ?>">
            <div>
                <?php 
                    if(isset($validationResult['error_message']['error_name'])){
                        echo "<p style='color: red; margin: 0px; padding-bottom: 5px;'>". $validationResult['error_message']['error_name']. "</p>";
                    }
                ?>
            </div>

            <label for="age">Age:</label>
            <input class="item-input" type="text" name="age" id="age" value="<?php echo isset($_POST['age']) ? $_POST['age'] : '' ?>">
            <div>
                <?php 
                    if(isset($validationResult['error_message']['error_age'])){
                        echo "<p style='color: red; margin: 0px; padding-bottom: 5px;'>". $validationResult['error_message']['error_age']. "</p>";
                    }
                ?>
            </div>

            <label for="email">Email:</label>
            <input class="item-input" type="text" name="email" id="email" value="<?php echo isset($_POST['email']) ? $_POST['email'] : '' ?>">
            <div>
                <?php 
                    if(isset($validationResult['error_message']['error_email'])){
                        echo "<p style='color: red; margin: 0px; padding-bottom: 5px;'>". $validationResult['error_message']['error_email']. "</p>";
                    }
                ?>
            </div>

            <label for="password">Password:</label>
            <input class="item-input" type="password" name="password" id="password" value="<?php echo isset($password) ? $password : '' ?>">
            <div>
                <?php 
                    if(isset($validationResult['error_message']['error_password'])){
                        echo "<p style='color: red; margin: 0px; padding-bottom: 5px;'>". $validationResult['error_message']['error_password']. "</p>";
                    }
                ?>
            </div>

            <label for="upload_image">Select image to upload:</label>

            <div class="img-uploader">
                <input class="uploader" type="file" name="upload_image" id="upload_image">
            </div>
            <div>
                <?php 
                    if(isset($validationResult['error_message']['error_upload'])){
                        echo "<p style='color: red; margin: 0px; padding-bottom: 5px;'>". $validationResult['error_message']['error_upload']. "</p>";
                    }
                ?>
            </div>

            <input class="btn-submit" type="submit" name="btn_submit" id="btn_submit" value="SUBMIT">

        </form>
    </div>
</body>
</html>