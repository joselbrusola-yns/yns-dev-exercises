<?php

// 3-5: Use the database in the applications that you developed.
// Same as exercises 1-6 to 1-13 but instead of csv, use the database.

// start session
session_start();

require 'database_config.php';
require 'database_model.php';
require 'validation.php';

define('DB_TABLE_NAME', 'userinfo');

// check if already login
if(!empty($_SESSION)){
    $targetPage = dirname($_SERVER['PHP_SELF']) . '/user_view.php';
    header("Location: $targetPage");
}

$email = '';
$password = '';

if (isset($_POST['btn_submit'])){

    // variable declaration
    // sanitize input
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_SPECIAL_CHARS);
    $password = preg_replace('/\s+/', ' ', filter_input(INPUT_POST, 'password', FILTER_SANITIZE_SPECIAL_CHARS)); // remove extra white space

    // validate inputs and uploads
    $validation = new Validation();
    $validationResult = $validation->validateCredentials($email, $password, $dbConnection);

}

// check if validation result is empty
if (empty($validationResult['error_message'])) {
    if($email && $password !== '') {
        $targetPage = dirname($_SERVER['PHP_SELF']) . '/user_view.php';
        header("Location: $targetPage");
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Log In</title>
    <style>
    .container {
        display: flex;
        justify-content: center;
    }

    .form-container {
        margin-top: 50px;
        padding: 20px;
        border: 1px solid black;
        width: 400px;

        display: flex;
        flex-direction: column;
    }

    .item-input {
        margin-bottom: 8px;
        padding: 5px;
    }

    .btn-submit {
        margin-top: 10px;
        margin-bottom: 10px;
        background-color: green;
        color: white;
        border: none;
        padding: 10px;
    }
</style>
</head>
<body>
    <div class="container">
        <form class="form-container" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" enctype="multipart/form-data">
            <h2 style="text-align:center; margin-top: 0;">Sign In</h2>

            <label for="email">Email</label>
            <input class="item-input" type="text" name="email" id="email" value="<?php echo isset($_POST['email']) ? $_POST['email'] : '' ?>">
            <div>
                <?php 
                    if(isset($validationResult['error_message']['error_email'])){
                        echo "<p style='color: red; margin: 0px; padding-bottom: 5px;'>". $validationResult['error_message']['error_email']. "</p>";
                    }
                ?>
            </div>

            <label for="password">Password</label>
            <input class="item-input"  type="password" name="password" id="password" value="<?php echo isset($_POST['password']) ? $_POST['password'] : '' ?>">
            <div>
                <?php 
                    if(isset($validationResult['error_message']['error_password'])){
                        echo "<p style='color: red; margin: 0px; padding-bottom: 5px;'>". $validationResult['error_message']['error_password']. "</p>";
                    }
                ?>
            </div>

            <input class="btn-submit" type="submit" name="btn_submit" id="btn_submit" value="LOGIN">

        </form>
    </div>
</body>
</html>