<?php 

class DatabaseModel
{
    public function selectAll($dbConnection, $tableName) {

        $sql = 'SELECT * FROM ' . $tableName . ';';
        $result = mysqli_query($dbConnection, $sql);
        $result_array = mysqli_fetch_all($result, MYSQLI_ASSOC);

        return $result_array;
    }

    public function selectSingleData($dbConnection, $tableName, $id) {

        $sql = 'SELECT * FROM ' . $tableName . ' where id = ' . $id . ';';
        $result = mysqli_query($dbConnection, $sql);
        $result_array = mysqli_fetch_all($result, MYSQLI_ASSOC);

        return $result_array;
    }

    public function selectPaginatedData($dbConnection, $tableName, $startPosition, $dataPerPage) {

        $sql = 'SELECT * FROM ' . $tableName . ' ORDER BY (id) ASC LIMIT ' . $startPosition . ', ' . $dataPerPage . ';';
        $result = mysqli_query($dbConnection, $sql);
        $result_array = mysqli_fetch_all($result, MYSQLI_ASSOC);

        return $result_array;
    }

    public function insert($dbConnection, $tableName, $name, $age, $email, $password, $imageDirectory) {

        $sql = $dbConnection->prepare("INSERT INTO ".$tableName." (name, age, email, password, image_directory) VALUES (?, ?, ?, ?, ?)");
        // The "sssss" argument lists the types of data that the parameters are. The s character tells mysql that the parameter is a string. (s-string, i-integer, d-double, b-BLOB)
        $sql->bind_param("sssss", $name, $age, $email, $password, $imageDirectory);
        $result = $sql->execute();

        return $result;
    }
}