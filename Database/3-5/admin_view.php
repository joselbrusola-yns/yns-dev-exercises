<?php 

// 3-5: Use the database in the applications that you developed.
// Same as exercises 1-6 to 1-13 but instead of csv, use the database.

// start session
session_start();

require 'database_config.php';
require 'database_model.php';
require 'validation.php';

define('DB_TABLE_NAME', 'userinfo');
define('DATA_PER_PAGE', 10);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin View</title>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid lightgrey;
    text-align: left;
    padding: 8px;
}

th {
    background-color: black;
    color: white;
}
</style>
</head>
<body>
    <table>
        <?php 
            // get total count
            $db = new DatabaseModel();
            $result = $db->selectAll($dbConnection, DB_TABLE_NAME);
            $resultCount = count($result);

            // set current page
            $currentPage = isset($_GET['page']) ? $_GET['page'] : 1;
            // set total pages
            $totalPages = ceil($resultCount / DATA_PER_PAGE);
            // set start position
            $startPosition = ($currentPage - 1) * DATA_PER_PAGE;

            // get paginated data
            $resultWithPagination = $db->selectPaginatedData($dbConnection, DB_TABLE_NAME, $startPosition, DATA_PER_PAGE);
            $resultWithPaginationCount = count($resultWithPagination);

            // get header
            $header = array_keys($resultWithPagination[0]);

            // create header
            echo '<tr>';
            foreach ($header as $th) {
                if ($th == 'image_directory') {
                    echo '<th>Image</th>';
                } else {
                    echo '<th>'.$th.'</th>';
                }
            }
            echo '</tr>';
            
            // populate body
            for ($i = 0; $i <= $resultWithPaginationCount - 1; $i++) {
                echo '<tr>';
                foreach ($resultWithPagination[$i] as $key => $val) {
                    if ($key == 'image_directory') {
                        echo '<td><img style="height: 100px; width: 100px;" src="' .$val. '"></td>';
                    } else {
                        echo '<td>'.$val.'</td>';
                    }
                }
                echo '</tr>';
            }
        ?>
    </table>
    <br>
    <div style="float:right; margin-bottom: 20px;">
        <?php 
            // for pagination
            for($i=1; $i <= $totalPages; $i++){
                echo '<a href="?page='.$i.'">Page'.$i.'</a> &nbsp';
            }
        ?>
    </div>
</body>
</html>