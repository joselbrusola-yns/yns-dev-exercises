<?php 

class Validation
{
    public function validateForm($name, $age, $email, $password, $dbConnection)
    {
        $errorMessage = [];
    
        // validate name
            // check if not empty
            if ($name == null || $name == ' ') {
                $errorMessage['error_name'] = "Name is required.";
            } else {
                // character only
                $validPattern = '/[^a-z ]/i';
                if (preg_match($validPattern, $name)) {
                    $errorMessage['error_name'] = "Invalid name: Letters and space only.";
                }
            }
        // validate age
            // check if not empty
            if ($age == null || $age === '') {
                $errorMessage['error_age'] = "Age is required.";
            } else {
                // is numeric
                if (!is_numeric($age)) {
                    $errorMessage['error_age'] = "Invalid age: Numbers only.";
                }else{
                    // check age length more than 4 is invalid
                    if(strlen($age) > 3){
                        $errorMessage['error_age'] = "Invalid age: Maximum age length is 3 digit only.";
                    }
    
                }
            }
            
        // validate email
            // check if not empty
            if ($email == null || $email == '') {
                $errorMessage['error_email'] = "Email is required.";
            } else {
                // validate email
                $validPattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
                if (!preg_match($validPattern, $email) == 1) {
                    $errorMessage['error_email'] = "Invalid email";
                } else {
                    // check if email already exists
                    $db = new DatabaseModel();
                    $result = $db->selectAll($dbConnection, DB_TABLE_NAME);
    
                    $resultCount = count($result);
    
                    for ($i = 0; $i <= $resultCount - 1; $i++) {
                        foreach ($result[$i] as $key => $val) {
                            if ($key == 'email') {
                                if (in_array($email, $result[$i])) {
                                    $errorMessage['error_email'] = "Email already exists.";
                                }
                            }
                            
                        }
                    }
                }
            }
    
        // validate password
            // check if not empty
            if ($password == null || $password == ' ') {
                $errorMessage['error_password'] = "Password is required.";
            }
    
        // handle upload files
        $upload_status = '';
        // validate if user choose file
        if(!empty($_FILES['upload_image']['name'])) {
    
            $uploadFileName = $_FILES['upload_image']['name'];
            $uploadTempName = $_FILES['upload_image']['tmp_name'];
            $uploadFileSize = $_FILES['upload_image']['size'];
    
            // create dir to upload and valid file extension 
            $allowedFileExtension = array('png', 'jpg', 'jpeg', 'gif');
            $targetDirectory = "uploads/";
    
            // get file ext
            $fileExtExploded = explode('.', $uploadFileName);
            $fileExt = strtolower(end($fileExtExploded));
    
            // finalize file name
            $currentDateTime = date('m-d-Y-His');
            $finalDirectory = $targetDirectory.$fileExtExploded[0].'_'.$currentDateTime.'.'.end($fileExtExploded);
    
            // validate file ext
            if(in_array($fileExt, $allowedFileExtension)){
                // validate file size im setting 1mb as max file size
                if($uploadFileSize <= 1000000){
                    // check if directory exists
                    if (!file_exists($targetDirectory)) {
                        mkdir($targetDirectory, 0777, true);
                    }
                    // upload file into a directory return true if success else return false
                    $upload_status = move_uploaded_file($uploadTempName, $finalDirectory);
                }else{
                    $errorMessage['error_upload'] = 'Invalid file uploaded: Filesize should not exceed 1mb';
                }
            }else{
                $errorMessage['error_upload'] = 'Invalid file uploaded: Allowed files extension are image only.';
            }
        }else{
            $errorMessage['error_upload'] = 'Upload file is required.';
        }
    
        // intialize result array
        $resultArray = [];
    
        // return error message if there is an error
        if($errorMessage){
            $resultArray['error_message'] = $errorMessage;
        }
    
        // return upload file data
        if($upload_status == true){
            $resultArray['upload_final_dir']['image_directory'] = $finalDirectory;
        }
    
        return $resultArray;
    
    }

    function validateCredentials($email, $password, $dbConnection)
    {
        $errorMessage = [];
            
        // validate email
            // check if not empty
            if ($email == null || $email == '') {
                $errorMessage['error_email'] = "Email is required.";
            } else {
                // validate email
                $validPattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
                if (!preg_match($validPattern, $email) == 1) {
                    $errorMessage['error_email'] = "Invalid email";
                }
            }
    
        // validate password
            // check if not empty
            if ($password == null || $password == ' ') {
                $errorMessage['error_password'] = "Password is required.";
            }
        
        // validate email and password
        
        // check if email exists
        $db = new DatabaseModel();
        $result = $db->selectAll($dbConnection, DB_TABLE_NAME);
    
        $resultCount = count($result);
    
        for ($i = 0; $i <= $resultCount - 1; $i++) {
            foreach ($result[$i] as $key => $val) {
                if ($key == 'email') {
                    if (in_array($email, $result[$i])) {
                        // get password from db
                        $resultPassword = $result[$i]['password'];
        
                        // email exists proceed on checking password if match
                        if (!password_verify($password, $resultPassword)) {
                            $errorMessage['error_password'] = "Invalid email or password.";
                        } else {
                            // set session data
                            $_SESSION["name"] = $result[$i]['name'];
                            $_SESSION["age"] = $result[$i]['age'];
                            $_SESSION["email"] = $result[$i]['email'];
                            $_SESSION["image_directory"] = $result[$i]['image_directory'];
                        }
                    }
                }
            }
        }
    
        // intialize result array
        $resultArray = [];
    
        // return error message if there is an error
        if($errorMessage){
            $resultArray['error_message'] = $errorMessage;
        }
    
        return $resultArray;
    
    }
        
}