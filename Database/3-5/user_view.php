<?php

// 3-5: Use the database in the applications that you developed.
// Same as exercises 1-6 to 1-13 but instead of csv, use the database.

// start session
session_start();

// check if session exist
if (!empty($_SESSION)) {
    // destroy session on logout
    if (isset($_POST['btn_logout'])) {
        session_destroy();
        $target_page = dirname($_SERVER['PHP_SELF']) . '/login.php';
        header("Location: $target_page");
    }
} else {
    // redirect to login page
    $target_page = dirname($_SERVER['PHP_SELF']) . '/login.php';
    header("Location: $target_page");
}
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User Profile</title>
</head>
<style>
    .container {
        display: flex;
    }

    .profile-details {
        padding-left: 50px;
    }

    .btn-submit {
        margin-top: 10px;
        background-color: red;
        color: white;
        border: none;
        padding: 10px;
    }

</style>
<body>

    <div class="container">
        <div class="profile"><?= '<img style="height: 300px; width: 300px;" src="' .$_SESSION["image_directory"]. '">' ?></div>
        <div class="profile-details">
            <h2>User Profile</h2>

            <?php
                echo '<p>Name: '.$_SESSION["name"].'</p>';
                echo '<p>Age: '.$_SESSION["age"].'</p>';
                echo '<p>Email: '.$_SESSION["email"].'</p>';
            ?>
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" >
                <input class="btn-submit" type="submit" name="btn_logout" id="btn_logout" value="LOGOUT">
            </form>
        </div>
    </div>

</body>
</html>